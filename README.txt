README file for the Photoblog Drupal module.


Description
***********

The Photoblog module provides simple photoblog-style features:

 * A single image per page.
 * Navigation links to the previous and next image (in chronological order).
 * Multiple photoblogs are possible by tagging posts with distinct categories.
 * Global photoblogs, or per-user photoblogs.
 * View the latest photoblog post on the site's frontpage.
 * A block with links to the latest post of each photoblog.

The accompanying Photoblog Views module hooks into Views to provide an "archive"
view: display photoblog posts by user, category, year, month, with corresponding
RSS feeds.

Of course, this module works with any kind of image. Photoblogs are only the
most common use for these features. Some other possible applications are online
slideshows and portfolios.

Why use this module when Views can provide something similar?

 * Simple previous/next navigation is more convenient than the standard pager.
 * Navigation links are permalinks (using node/nid, or corresponding url
   aliases if any).
 * Visitors can post comments in the same page as the photoblog posts, and
   without losing the navigation context.


Requirements
************

This module requires Drupal 5.

It also requires the following modules:

 * Image (http://drupal.org/project/image)
 * Taxonomy (part of the Drupal core)

The optional Photoblog Views module requires the following modules:

 * Views (http://drupal.org/project/views)
 * Views RSS (http://drupal.org/project/views)

The following modules are not required, but are recommended:

 * Comment (part of the Drupal core)
   Allow visitors to comment your photos.
 * Image Gallery (http://drupal.org/project/image)
   Cross-post in both your photoblog(s) and nicely organized galleries.
 * Tagadelic (http://drupal.org/project/tagadelic)
   Tag your posts with relevant keywords.
 * Exif (http://drupal.org/project/exif)
   Display shooting data along with each photo.
 * Pathauto (http://drupal.org/project/pathauto)
   Get nice url aliases effortlessly.
 * Scheduler (http://drupal.prg/project/scheduler)
   Prepare some posts in advance (and take a vacation).
 * Taxonomy Hide (http://drupal.org/project/taxonomy_hide)
   Hide those terms you may not really want to show along with each post, such as
   the photoblog and the image gallery terms.


Installation
************

1. Install and enable the required modules (following each module's installation
   instructions).

2. Extract the 'photoblog' module directory into your Drupal modules directory.

3. Enable the Photoblog module on your site's Administer > Site building >
   Modules page.

4. Go to Administer > User management > Access control, and give the "administer
   photoblogs" permission to those roles that need to be able to create
   photoblogs. Note that ordinary photobloggers need only the "create images"
   permission.

5. Go to Administer > Content management > Photoblogs, and add a photoblog. More
   than one photoblog is allowed, if you wish. Each will have its own series of
   photos.

6. Go to Administer > Site configuration > Photoblog, and:

   a. If you want your photoblog posts to occupy your site's front page, check
      "Automatically set the front page to latest post".

   b. Select the desired type of navigation.

      * If you want each user to have his own separate photoblogs, choose
        "Per-user".

      * If you want users to share common photoblogs, choose "Global". This is
        the default. If only one user is going to post, leave it as "Global".

   c. Select where you want to output the navigation links (previous/next links
      for each post). Choose the most appropriate for your theming needs. If
      unsure, choose "Links".

      * "Block" will create a block containing the links (you will need to
        enable the block in Administer > Site building > Blocks). "

      * "Content" will display the links along with the rest of each node's
        content.

      * "Links" will display the links as standard node links (same location as
        "read more" and "thumbnail" links, for example). This is the default.

7. Go to Administer > Site building > Blocks, and enable the "Photoblogs list"
   block. This is not really necessary if you have only one photoblog and have
   enabled "Automatically set the front page to latest post". This block
   provides easy entry points to your photoblogs, especially if you have more
   than one photoblog, or if the posts are not taken to the front page.

   The entry point of a photoblog will be either its first post or its latest
   post, depending on the photoblog's settings (in Administer > Content
   management > Photoblogs).

8. Optional: Install the Views module and enable Views, Views RSS and Photoblog
   Views. A "photoblog_posts" default view will become available. It supports
   user, category, year, month and "feed" arguments.

   See the Handbooks for more documentation about Views
   (http://drupal.org/node/54432).


Usage
*****

To post in your photoblog, simply create an image (with Create content > Image),
and select a photoblog in the content submission form. Only one photoblog is
allowed for any given image.

The image can still be assigned various tags, categories, or image
galleries. These won't interfere with the photoblog navigation.


Theming
*******

You may want to theme the previous/next links. This can be done by overriding
the theme_photoblog_prev() and theme_photoblog_next() functions (from
photoblog.module) in your theme's template.php. See the Handbooks on theming
(http://drupal.org/node/509).

If you are using Photoblog Views, you will also want to theme the default view's
appearance, because a list presentation is not really interesting for photos.
Some CSS work will be needed for this.


Credits
*******

David Lesieur, http://davidlesieur.com, http://drupal.org/user/17157


